/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   frame.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 01:25:38 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:14:39 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAME_H
# define FRAME_H
# include "point.h"
# include <mlx.h>
# include <point.h>

typedef struct s_frame
{
	struct s_frame	*p;
	void			*img;
	char			*addr;
	int				bpp;
	int				ll;
	int				endian;
	t_point_2d		off;
	t_point_2d		size;
}					t_frame;

int					frame_init(t_frame *f, void *mlx, t_point_2d size);
int					frame_pixel_put(t_frame frame, t_point_2d p);
int					frame_line(t_frame frame, t_bound_2d b);
int					frame_square(t_frame f, t_point_2d p, int size);
int					frame_rectangle_size(t_frame f, t_point_2d p, int h, int v);
int					frame_rectangle_bound(t_frame f, t_bound_2d b);
int					frame_background(t_frame frame, int color);
void				frame_close(t_frame *f, void *mlx);

int					frame_error_mlx_image(void);
int					frame_error_mlx_addr(void);

#endif
