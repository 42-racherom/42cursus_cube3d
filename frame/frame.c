/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   frame.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/22 01:25:23 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 10:41:40 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "frame.h"
#include <math.h>
#include <point.h>

int	frame_pixel_put(t_frame f, t_point_2d p)
{
	char	*dst;

	if (p.x < 0 || p.x >= f.size.x || p.y < 0 || p.y >= f.size.y)
		return (0);
	if (!f.addr)
		return (-1);
	dst = f.addr + (((int)p.y * f.ll) + ((int)p.x * (f.bpp / 8)));
	*(unsigned int *)dst = p.c;
	return (0);
}

int	frame_line(t_frame f, t_bound_2d b)
{
	t_point_2d	d;
	t_point_2d	p;
	float		s;
	int			r;

	d.x = b.max.x - b.min.x;
	d.y = b.max.y - b.min.y;
	s = fabsf(d.y);
	if (fabsf(d.x) >= s)
		s = fabsf(d.x);
	d.x /= s;
	d.y /= s;
	p = b.min;
	r = 0;
	while (!r && in_bound_floor_2d(b, p))
	{
		r = frame_pixel_put(f, p);
		p.x += d.x;
		p.y += d.y;
	}
	return (r);
}

int	frame_background(t_frame frame, int color)
{
	t_bound_2d	b;

	b.min.x = 0;
	b.min.y = 0;
	b.min.c = color;
	b.max = frame.size;
	return (frame_rectangle_bound(frame, b));
}
