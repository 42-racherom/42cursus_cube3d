/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rectangle.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/13 17:37:30 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 10:41:25 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <frame.h>
#include <point.h>

int	frame_rectangle_bound(t_frame f, t_bound_2d b)
{
	t_point_2d	p;

	bound_2d_fix(&b);
	b.max.x = fminf(b.max.x, f.size.x);
	b.max.y = fminf(b.max.y, f.size.y);
	if (f.p)
		return (frame_rectangle_bound(*f.p, offset_bound_2d(b, f.off)));
	p = b.min;
	while (p.y < b.max.y)
	{
		p.x = b.min.x;
		while (p.x < b.max.x)
		{
			frame_pixel_put(f, p);
			p.x++;
		}
		p.y++;
	}
	return (0);
}

int	frame_rectangle_size(t_frame f, t_point_2d p, int h_size, int v_size)
{
	t_bound_2d	b;
	int			i;
	int			j;
	t_point_2d	d;

	b.min = p;
	b.max.x = p.x + h_size;
	b.max.y = p.y + v_size;
	return (frame_rectangle_bound(f, b));
	i = 0;
	j = 0;
	while (i < v_size)
	{
		while (j < h_size)
		{
			d.c = p.c;
			d.x = p.x + j;
			d.y = p.y + i;
			frame_pixel_put(f, d);
			j++;
		}
		j = 0;
		i++;
	}
	return (0);
}

int	frame_square(t_frame f, t_point_2d p, int size)
{
	t_bound_2d	b;

	b.min = p;
	b.max.x = p.x + size;
	b.max.y = p.y + size;
	return (frame_rectangle_bound(f, b));
}
