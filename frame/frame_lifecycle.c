/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   frame_lifecycle.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/09 22:15:44 by racherom          #+#    #+#             */
/*   Updated: 2024/05/30 16:16:05 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "frame.h"
#include <mlx.h>
#include <stdlib.h>

int	frame_init(t_frame *f, void *mlx, t_point_2d size)
{
	f->img = mlx_new_image(mlx, size.x, size.y);
	if (!f->img)
		return (frame_error_mlx_image());
	f->addr = mlx_get_data_addr(f->img, &f->bpp, &f->ll, &f->endian);
	if (!f->addr)
	{
		mlx_destroy_image(mlx, f->img);
		return (frame_error_mlx_addr());
	}
	f->p = 0;
	f->size = size;
	f->off.x = 0;
	f->off.y = 0;
	return (0);
}

void	frame_close(t_frame *f, void *mlx)
{
	if (f->img)
		mlx_destroy_image(mlx, f->img);
}
