/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/30 16:12:27 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:14:17 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	frame_error_mlx_image(void)
{
	write(2, "Error\nFrame: Failed to create new MLX image\n", 44);
	return (1);
}

int	frame_error_mlx_addr(void)
{
	write(2, "Error\nFrame: Failed to get address of MLX image\n", 48);
	return (1);
}
