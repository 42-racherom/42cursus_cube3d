/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/13 18:13:41 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 08:30:17 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER_H
# define PLAYER_H
# include <point.h>

typedef struct s_player
{
	t_point_2d	pos;
	float		r;
	int			rdeg;
}				t_player;

int				player_init(t_player *p);
void			player_clear(t_player *p);

#endif
