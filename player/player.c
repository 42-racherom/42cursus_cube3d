/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/13 17:50:00 by rauer             #+#    #+#             */
/*   Updated: 2024/05/31 21:32:59 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "player.h"
#include <map.h>
#include <ray.h>

int	player_init(t_player *p)
{
	p->pos.x = 0;
	p->pos.y = 0;
	p->pos.c = 0;
	p->rdeg = 0;
	return (0);
}

int	player_move(t_player *p, t_map *m, int r, float speed)
{
	t_ray		ray;
	t_point_2d	p;

	ray = cast_rays(m, p->pos, p->rdeg + r);
}

void	player_clear(t_player *p)
{
	(void)p;
}
