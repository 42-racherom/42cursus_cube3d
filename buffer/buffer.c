/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buffer.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/19 04:16:45 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 10:36:06 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "buffer.h"
#include <stdio.h>
#include <stdlib.h>
#include <str.h>
#include <unistd.h>

int	buffer_error_read(void)
{
	perror("Error:\nBuffer");
	return (1);
}

int	buffer_fill(t_buffer *b)
{
	if (b->i < b->l)
		return (0);
	b->l = read(b->fd, b->a, BUFFERSIZE);
	if (b->l < 0)
		return (buffer_error_read());
	b->i = 0;
	return (0);
}

int	buffer_skip_whitespaces(t_buffer *b)
{
	int	r;

	r = buffer_fill(b);
	if (r)
		return (r);
	while (!r && (b->l > 0 || b->a[b->i] == ' ' || b->a[b->i] == '\t'
			|| b->a[b->i] == '\n' || b->a[b->i] == '\r'))
	{
		b->i++;
		r = buffer_fill(b);
	}
	return (r);
}

int	buffer_toi(t_buffer *b, unsigned int *i)
{
	if (buffer_fill(b) || !b->l || b->a[b->i] < '0' || b->a[b->i] > '9')
		return (1);
	while (b->i < b->l && b->a[b->i] >= '0' && b->a[b->i] <= '9')
	{
		*i *= 10;
		*i += b->a[b->i] - '0';
		b->i++;
		if (buffer_fill(b))
			return (1);
	}
	return (0);
}

t_string	buffer_gnl_string(t_buffer *b)
{
	t_string	str;
	int			l;

	str = str_make(0);
	while (!buffer_fill(b) && b->i < b->l)
	{
		l = b->i;
		while (l < b->l && b->a[l] && b->a[l] != '\n')
			l++;
		l = str_extend(&str, l - b->i);
		while (!l && b->i < b->l && b->a[b->i] != '\n')
			l = str_putchar(&str, b->a[b->i++]);
		if (l)
		{
			str_clear(&str);
			break ;
		}
		if (b->i < b->l)
		{
			b->i++;
			break ;
		}
	}
	return (str);
}
