/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buffer.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/19 04:12:16 by rauer             #+#    #+#             */
/*   Updated: 2024/05/29 03:58:31 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUFFER_H
# define BUFFER_H
# ifndef BUFFERSIZE
#  define BUFFERSIZE 1024
# endif
# include <str.h>

typedef struct s_buffer
{
	int		fd;
	char	a[BUFFERSIZE];
	int		i;
	int		l;
}			t_buffer;

int			buffer_fill(t_buffer *b);
int			buffer_skip_whitespaces(t_buffer *b);
t_string	buffer_gnl_string(t_buffer *b);
int			buffer_toi(t_buffer *b, unsigned int *i);

#endif
