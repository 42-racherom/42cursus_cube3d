/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/20 01:16:12 by rauer             #+#    #+#             */
/*   Updated: 2024/05/14 06:05:07 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef POINT_H
# define POINT_H
# include <math.h>

typedef struct s_point_int_2d
{
	int			x;
	int			y;
	int			c;
}				t_point_int_2d;

typedef struct s_point_2d
{
	float		x;
	float		y;
	int			c;
}				t_point_2d;

typedef struct s_point_3d
{
	float		x;
	float		y;
	float		z;
	int			c;

}				t_point_3d;

typedef struct s_bound_2d
{
	t_point_2d	min;
	t_point_2d	max;
}				t_bound_2d;

typedef struct s_bound_3d
{
	t_point_3d	min;
	t_point_3d	max;
}				t_bound_3d;

t_point_2d		empty_point_2d(void);
t_bound_2d		empty_bound_2d(void);
t_point_2d		size_2d(t_bound_2d b);
t_point_2d		center_2d(t_bound_2d b);
t_point_2d		distance_point_2d(t_point_2d p1, t_point_2d p2);
t_point_2d		offset_point_2d(t_point_2d p, t_point_2d o);
t_bound_2d		offset_bound_2d(t_bound_2d b, t_point_2d o);
t_point_2d		scale_point_2d(t_point_2d p1, float f);
t_bound_2d		scale_bound_2d(t_bound_2d b1, float f);
void			bound_2d_fix(t_bound_2d *b);
void			bound_2d_extend(t_bound_2d *b, t_point_2d p);
t_point_3d		center_3d(t_bound_3d b);
t_point_3d		empty_point_3d(void);
t_bound_3d		empty_bound_3d(void);
void			update_bound_3d(t_bound_3d *b, t_point_3d p);
float			distance_2d(t_point_2d p1, t_point_2d p2);
int				in_bound_floor_2d(t_bound_2d b, t_point_2d p);
t_point_3d		rotate_z(t_point_3d p, t_point_3d o, float r);
t_point_2d		rotate_2d(t_point_2d p, t_point_2d o, float r);

#endif
