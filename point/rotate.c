/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/04 05:19:32 by racherom          #+#    #+#             */
/*   Updated: 2024/05/10 00:07:41 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <point.h>

t_point_2d	rotate_2d(t_point_2d p, t_point_2d o, float r)
{
	t_point_2d	pr;

	if (r == 0)
		return (p);
	p.x -= o.x;
	p.y -= o.y;
	pr.x = p.x * cos(r) - p.y * sin(r);
	pr.y = p.x * sin(r) + p.y * cos(r);
	pr.x += o.x;
	pr.y += o.y;
	pr.c = p.c;
	return (pr);
}

t_point_3d	rotate_x(t_point_3d p, t_point_3d o, float r)
{
	if (r == 0)
		return (p);
	p.y -= o.y;
	p.y = p.y * cos(r) - p.z * sin(r);
	p.y += o.y;
	p.z -= o.z;
	p.z = p.y * sin(r) + p.z * cos(r);
	p.z += o.z;
	return (p);
}

t_point_3d	rotate_z(t_point_3d p, t_point_3d o, float r)
{
	t_point_3d	pr;

	if (r == 0)
		return (p);
	p.x -= o.x;
	p.y -= o.y;
	pr.x = p.x * cos(r) - p.y * sin(r);
	pr.y = p.x * sin(r) + p.y * cos(r);
	pr.x += o.x;
	pr.y += o.y;
	pr.z = p.z;
	pr.c = p.c;
	return (pr);
}
