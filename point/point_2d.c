/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point_2d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/02 19:08:16 by racherom          #+#    #+#             */
/*   Updated: 2024/05/30 12:16:03 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <point.h>

t_point_2d	offset_point_2d(t_point_2d p, t_point_2d o)
{
	p.x += o.x;
	p.y += o.y;
	return (p);
}

t_point_2d	scale_point_2d(t_point_2d p1, float f)
{
	t_point_2d	p2;

	p2.c = p1.c;
	p2.x = p1.x * f;
	p2.y = p1.y * f;
	return (p2);
}

t_point_2d	distance_point_2d(t_point_2d p1, t_point_2d p2)
{
	p2.x -= p1.x;
	p2.y -= p1.y;
	return (p2);
}

float	distance_2d(t_point_2d p1, t_point_2d p2)
{
	return (hypotf(p2.x - p1.x, p2.y - p1.y));
}
