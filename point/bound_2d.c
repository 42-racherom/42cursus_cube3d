/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bound_2d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/02 18:48:49 by racherom          #+#    #+#             */
/*   Updated: 2024/05/30 12:28:49 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <point.h>

void	bound_2d_fix(t_bound_2d *b)
{
	float	tmp;

	tmp = fminf(b->min.x, b->max.x);
	b->max.x = fmaxf(b->min.x, b->max.x);
	b->min.x = tmp;
	tmp = fminf(b->min.y, b->max.y);
	b->max.y = fmaxf(b->min.y, b->max.y);
	b->min.y = tmp;
}

t_bound_2d	offset_bound_2d(t_bound_2d b, t_point_2d o)
{
	b.min = offset_point_2d(b.min, o);
	b.max = offset_point_2d(b.max, o);
	return (b);
}

t_bound_2d	scale_bound_2d(t_bound_2d b1, float f)
{
	t_bound_2d	b2;

	b2.min = scale_point_2d(b1.min, f);
	b2.max = scale_point_2d(b1.max, f);
	return (b2);
}

int	in_bound_floor_2d(t_bound_2d b, t_point_2d p)
{
	return (((p.x >= floor(b.min.x) && p.x < floor(b.max.x) + 1)
			|| (p.x < floor(b.min.x) + 1 && p.x >= floor(b.max.x)))
		&& ((p.y >= floor(b.min.y) && p.y < floor(b.max.y) + 1)
			|| (p.y < floor(b.min.y) + 1 && p.y >= floor(b.max.y))));
}
