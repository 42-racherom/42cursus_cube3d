/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_ray.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/30 12:01:59 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 12:14:04 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ray.h"
#include "render.h"
#include <math.h>
#include <point.h>

t_point_2d	texture_point(t_point_2d p, t_orientation o)
{
	t_point_2d	tp;

	tp.y = 0;
	if (o == N)
		tp.x = p.x;
	else if (o == E)
		tp.x = p.y;
	else if (o == S)
		tp.x = -p.x;
	else if (o == W)
		tp.x = -p.y;
	return (tp);
}

int	render_texture(t_renderer *r, t_bound_2d b, t_ray ray, float h)
{
	t_point_2d	tp;
	t_point_2d	dp;

	tp = texture_point(ray.b.max, ray.o);
	dp.y = b.min.y;
	if (dp.y < 0)
	{
		tp.y += h * fabsf(dp.y);
		dp.y = 0;
	}
	while (dp.y < b.max.y && dp.y < r->f_game.size.y)
	{
		dp.x = b.min.x;
		dp.c = texture_at(r->t + ray.o, tp);
		while (dp.x < b.max.x)
		{
			if (frame_pixel_put(r->f_game, dp))
				return (1);
			dp.x++;
		}
		dp.y++;
		tp.y += h;
	}
	return (0);
}

int	render_ray_3d(t_renderer *r, t_ray ray, float *x)
{
	t_bound_2d	db;
	float		h;

	h = r->f_game.size.x / ray.d;
	db.min.y = r->f_game.size.y / 2 - h / 2;
	db.min.x = *x;
	*x += r->cd;
	db.max.x = *x;
	db.max.y = db.min.y + h;
	return (render_texture(r, db, ray, 1 / h));
}

int	render_rays_3d(t_renderer *r, t_player *p, t_map *m)
{
	t_ray	ray;
	float	rad;
	float	x;
	int		i;

	rad = p->rdeg - (r->fov / 2);
	if (rad < 0)
		rad += 360;
	i = 0;
	x = 0;
	while (i < r->n_ray)
	{
		ray = cast_rays(m, p->pos, rad);
		r->rays[i++] = ray.b.max;
		ray.d *= cosf(fabsf(p->rdeg - rad) * M_PI / 180);
		if (render_ray_3d(r, ray, &x))
			return (1);
		rad += r->rd;
		if (rad >= 360)
			rad -= 360;
	}
	return (0);
}
