/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   renderer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/29 06:53:46 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:09:46 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <frame.h>
#include <map.h>
#include <math.h>
#include <player.h>
#include <render.h>
#include <stdio.h>
#include <stdlib.h>
#include <texture.h>

int	renderer_init(t_renderer *r)
{
	int	i;

	r->c = 0xffffff;
	r->f = 0;
	r->fov = 60;
	r->rays = 0;
	r->w_game = 0;
	r->w_minimap = 0;
	r->f_game.img = 0;
	r->f_minimap.img = 0;
	i = 0;
	while (i < 4)
		r->t[i++].img = 0;
	r->mlx = mlx_init();
	if (!r->mlx)
		return (render_error_mlx());
	mlx_do_key_autorepeatoff(r->mlx);
	return (0);
}

int	renderer_start(t_renderer *r)
{
	t_point_2d	p;

	p.x = 1920;
	p.y = 1080;
	r->f_game.size = p;
	if (frame_init(&r->f_game, r->mlx, p))
		return (1);
	r->w_game = mlx_new_window(r->mlx, p.x, p.y, "Cub3D");
	if (!r->w_game)
		return (render_error_mlx_window());
	r->n_ray = p.x;
	if (r->n_ray > p.x)
		r->n_ray = p.x;
	r->rd = (float)r->fov / (float)r->n_ray;
	r->cd = p.x / r->n_ray;
	r->rays = malloc(r->n_ray * sizeof(t_point_2d));
	if (!r->rays)
		return (parser_error_malloc());
	return (0);
}

int	renderer_start_minimap(t_renderer *r, t_map *m)
{
	t_point_2d	p;

	r->cell_size = fminf(1920 / m->l, 1080 / m->max_l);
	p.x = r->cell_size * m->max_l;
	p.y = r->cell_size * m->l;
	if (frame_init(&r->f_minimap, r->mlx, p))
		return (1);
	r->w_minimap = mlx_new_window(r->mlx, p.x, p.y, "Minimap");
	if (!r->w_minimap)
		return (render_error_mlx_window());
	return (0);
}

void	renderer_clear(t_renderer *r)
{
	int	i;

	if (r->rays)
		free(r->rays);
	if (r->w_game)
		mlx_destroy_window(r->mlx, r->w_game);
	if (r->w_minimap)
		mlx_destroy_window(r->mlx, r->w_minimap);
	frame_close(&r->f_game, r->mlx);
	frame_close(&r->f_minimap, r->mlx);
	i = 0;
	while (i < 4)
		texture_clear(r->mlx, r->t + i++);
}
