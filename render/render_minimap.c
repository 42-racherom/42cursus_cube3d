/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_minimap.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/13 18:05:00 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 10:41:40 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <frame.h>
#include <map.h>
#include <player.h>
#include <render.h>
#include <stdio.h>

int	render_minimap_rays(t_renderer *r, t_player *p)
{
	t_bound_2d	draw;
	int			i;

	draw.max = scale_point_2d(p->pos, r->cell_size);
	i = 0;
	while (i < r->n_ray)
	{
		draw.min = scale_point_2d(r->rays[i], r->cell_size);
		frame_line(r->f_minimap, draw);
		i++;
	}
	return (0);
}

int	render_minimap_player(t_renderer *r, t_player *player)
{
	t_point_2d	p;
	int			err;

	p.x = -0.125;
	p.y = -0.125;
	p = scale_point_2d(offset_point_2d(player->pos, p), r->cell_size);
	err = frame_square(r->f_minimap, p, r->cell_size / 4);
	if (!err)
		err = render_minimap_rays(r, player);
	return (err);
}

int	render_minimap(t_renderer *r, t_map *m)
{
	int			i;
	int			j;
	int			f;
	t_point_2d	p;

	i = 0;
	p.y = 1;
	while (i < m->l)
	{
		j = m->rows[i].off;
		p.x = 1 + m->rows[i].off * r->cell_size;
		while (j < m->max_l)
		{
			p.c = 0xaaaaaa;
			f = map_get(m, j++, i);
			if (f == 1)
				p.c = 0x555555;
			if (f >= 0)
				frame_square(r->f_minimap, p, r->cell_size - 2);
			p.x += r->cell_size;
		}
		i++;
		p.y += r->cell_size;
	}
	return (0);
}

int	renderer_minimap(t_renderer *r, t_player *p, t_map *m)
{
	int	err;

	err = frame_background(r->f_minimap, 0);
	if (!err)
		err = render_minimap(r, m);
	if (!err)
		err = render_minimap_player(r, p);
	if (!err)
		mlx_clear_window(r->mlx, r->w_minimap);
	if (!err)
		mlx_put_image_to_window(r->mlx, r->w_minimap, r->f_minimap.img, 0, 0);
	return (err);
}
