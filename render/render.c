/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/29 17:33:59 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 12:04:05 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "render.h"
#include <frame.h>
#include <map.h>
#include <player.h>
#include <stdio.h>
#include <stdlib.h>

int	background_3d(t_frame f, unsigned int ce, unsigned int fl)
{
	t_bound_2d	b;

	b.min.x = 0;
	b.min.y = 0;
	b.min.c = ce;
	b.max.x = f.size.x;
	b.max.y = f.size.y / 2;
	frame_rectangle_bound(f, b);
	b.min.y = b.max.y;
	b.min.c = fl;
	b.max.y = f.size.y;
	frame_rectangle_bound(f, b);
	return (0);
}

int	renderer_game(t_renderer *r, t_player *p, t_map *m)
{
	int	ret;

	ret = background_3d(r->f_game, r->c, r->f);
	if (!ret)
		ret = render_rays_3d(r, p, m);
	if (!ret)
		mlx_clear_window(r->mlx, r->w_game);
	if (!ret)
		mlx_put_image_to_window(r->mlx, r->w_game, r->f_game.img, 0, 0);
	return (ret);
}
