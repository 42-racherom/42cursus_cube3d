/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/29 06:32:19 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:18:50 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RENDER_H
# define RENDER_H

# include <frame.h>
# include <map.h>
# include <player.h>
# include <texture.h>

typedef struct s_renderer
{
	t_point_2d		*rays;
	void			*mlx;
	void			*w_game;
	void			*w_minimap;
	t_frame			f_game;
	t_frame			f_minimap;
	t_texture		t[4];
	unsigned int	f;
	unsigned int	c;
	int				n_ray;
	int				fov;
	float			rd;
	float			cd;
	float			cell_size;
}					t_renderer;

int					renderer_init(t_renderer *r);
int					renderer_start(t_renderer *r);
int					renderer_start_minimap(t_renderer *r, t_map *m);
int					renderer_game(t_renderer *r, t_player *p, t_map *m);
int					render_rays_3d(t_renderer *r, t_player *p, t_map *m);
int					renderer_minimap(t_renderer *r, t_player *p, t_map *m);
void				renderer_clear(t_renderer *r);

int					render_error_mlx(void);
int					render_error_mlx_window(void);
int					parser_error_malloc(void);

#endif
