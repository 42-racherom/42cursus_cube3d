/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/30 16:02:27 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:11:07 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	render_error_mlx(void)
{
	write(2, "Error\nFailed to inizialize MLX\n", 31);
	return (1);
}

int	render_error_mlx_window(void)
{
	write(2, "Error\nFailed to open new MLX window\n", 36);
	return (1);
}

int	render_error_malloc_rays(void)
{
	write(2, "Error\nFailed to allocate memmory for the rays\n", 46);
	return (1);
}
