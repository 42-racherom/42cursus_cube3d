/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/27 04:48:05 by racherom          #+#    #+#             */
/*   Updated: 2024/05/30 05:34:32 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXTURE_H
# define TEXTURE_H
# include "mlx.h"
# include <point.h>

typedef struct s_texture
{
	void			*img;
	char			*addr;
	int				s;
	int				w;
	int				h;
	int				ll;
	int				bpp;
	int				endian;
	unsigned int	bg;
}					t_texture;

int					texture_from_xpm(void *mlx, t_texture *t, char *file);
int					parse_ppm_file_texture(t_texture *t, char *str);
unsigned int		texture_at(t_texture *texture, t_point_2d p);
void				texture_clear(void *mlx, t_texture *t);

#endif
