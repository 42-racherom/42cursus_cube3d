/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/29 16:56:18 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:34:15 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "texture.h"
#include <mlx.h>
#include <point.h>
#include <stdlib.h>
#include <unistd.h>

int	texture_error_file(void)
{
	write(2, "Error\nTexture: invalid texture file\n", 36);
	return (1);
}

int	texture_error_mlx_addr(void)
{
	write(2, "Error\nTexture: Failed to get address of MLX image\n", 50);
	return (1);
}

int	texture_from_xpm(void *mlx, t_texture *t, char *file)
{
	t->img = mlx_xpm_file_to_image(mlx, file, &t->w, &t->h);
	if (!t->img)
		return (texture_error_file());
	t->s = t->w;
	if (t->h < t->s)
		t->s = t->h;
	t->addr = mlx_get_data_addr(t->img, &t->bpp, &t->ll, &t->endian);
	if (!t->addr)
	{
		mlx_destroy_image(mlx, t->img);
		return (texture_error_mlx_addr());
	}
	return (0);
}

unsigned int	texture_at(t_texture *t, t_point_2d p)
{
	char	*dst;

	p.x -= floorf(p.x);
	p.y -= floorf(p.y);
	if (p.x < 0)
		p.x = 1 - p.x;
	if (p.y < 0)
		p.y = 1 - p.y;
	p = scale_point_2d(p, t->s);
	dst = t->addr + (((int)p.y) * t->ll) + (((int)p.x) * (t->bpp / 8));
	return (*(unsigned int *)dst);
}

void	texture_clear(void *mlx, t_texture *t)
{
	if (t->img)
		mlx_destroy_image(mlx, t->img);
	t->img = 0;
	t->addr = 0;
}
