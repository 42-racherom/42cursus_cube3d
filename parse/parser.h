/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/27 21:24:36 by racherom          #+#    #+#             */
/*   Updated: 2024/05/30 16:35:34 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H
# include <app.h>
# include <buffer.h>

typedef struct s_parser_line
{
	t_string				line;
	struct s_parser_line	*next;
}							t_parser_line;

typedef struct s_parser
{
	t_buffer				b;
	t_parser_line			*lines;
	t_parser_line			*last;
	int						l;
}							t_parser;

int							parse_cub(t_app *app, char *str);
int							parse_add_line(t_parser *p, t_string s);
void						parse_pop(t_parser_line **l, int f);
int							parse_rgb_texture(char *str, unsigned int *rgb);
int							parse_textures(t_parser *p, t_app *app);
int							parse_map(t_app *app, t_parser *p);

int							parser_error_file(void);
int							parser_error_cub(void);
int							parser_error_malloc(void);

#endif
