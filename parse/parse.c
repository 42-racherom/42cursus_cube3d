/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/27 21:56:35 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:24:22 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>
#include <fcntl.h>
#include <mlx.h>
#include <parser.h>
#include <stdlib.h>
#include <unistd.h>

void	parse_pop(t_parser_line **l, int f)
{
	t_parser_line	*next;

	next = (*l)->next;
	if (f)
		str_clear(&(*l)->line);
	free(*l);
	*l = next;
}

void	parser_free(t_parser *p)
{
	while (p->lines)
		parse_pop(&p->lines, 1);
}

int	parse_add_line(t_parser *p, t_string s)
{
	t_parser_line	*l;

	if (!s.l)
	{
		free(s.s);
		return (0);
	}
	l = malloc(sizeof(t_parser_line));
	if (!l)
		return (parser_error_malloc());
	l->line = s;
	l->next = 0;
	if (!p->lines)
		p->lines = l;
	else
		p->last->next = l;
	p->last = l;
	p->l++;
	return (0);
}

int	parse_cub(t_app *app, char *str)
{
	t_parser	p;
	int			r;

	p.l = 0;
	p.last = 0;
	p.lines = 0;
	p.b.fd = open(str, O_RDONLY);
	if (p.b.fd < 0)
		return (parser_error_file());
	p.b.i = 0;
	p.b.l = 0;
	r = parse_textures(&p, app);
	if (!r)
		r = parse_map(app, &p);
	close(p.b.fd);
	return (r);
}
