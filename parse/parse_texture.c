/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_texture.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/27 15:30:40 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:30:55 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include <app.h>
#include <buffer.h>
#include <stdio.h>
#include <str.h>

int	parse_texture(void *mlx, t_texture *t, char *str)
{
	while (*str == ' ' || *str == '\t')
		str++;
	if (texture_from_xpm(mlx, t, str))
		return (1);
	return (0);
}

int	parse_texture_select(t_app *app, t_string s)
{
	int	r;

	r = -1;
	if (s.l < 3)
		return (r);
	if (s.s[0] == 'N' && s.s[1] == 'O')
		r = parse_texture(app->r.mlx, app->r.t + N, s.s + 2);
	else if (s.s[0] == 'S' && s.s[1] == 'O')
		r = parse_texture(app->r.mlx, app->r.t + S, s.s + 2);
	else if (s.s[0] == 'W' && s.s[1] == 'E')
		r = parse_texture(app->r.mlx, app->r.t + W, s.s + 2);
	else if (s.s[0] == 'E' && s.s[1] == 'A')
		r = parse_texture(app->r.mlx, app->r.t + E, s.s + 2);
	else if (s.s[0] == 'F')
		r = parse_rgb_texture(s.s + 1, &app->r.f);
	else if (s.s[0] == 'C')
		r = parse_rgb_texture(s.s + 1, &app->r.c);
	return (r);
}

int	parse_textures(t_parser *p, t_app *app)
{
	int			r;
	t_string	s;

	r = 0;
	app->r.f = 0;
	app->r.c = 0xFFFFFF;
	while (!r)
	{
		s = buffer_gnl_string(&p->b);
		if (s.l > 0)
			r = parse_texture_select(app, s);
		if (r < 0 || !s.s)
			break ;
		str_clear(&s);
	}
	if (!s.s || r >= 0)
		return (1);
	if (!app->r.t[0].img || !app->r.t[1].img
		|| !app->r.t[2].img || !app->r.t[3].img)
	{
		str_clear(&s);
		return (parser_error_cub());
	}
	return (parse_add_line(p, s));
}
