/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/27 15:39:36 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 09:54:40 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"
#include <app.h>
#include <buffer.h>
#include <map.h>
#include <player.h>
#include <stdio.h>
#include <stdlib.h>
#include <str.h>

int	parse_check_map_row(t_map *m, int i)
{
	int	j;
	int	a[3];
	int	l;

	j = 0;
	l = m->rows[i].l;
	if (i > 0 && m->rows[i - 1].l > m->rows[i].l)
		l = m->rows[i - 1].l;
	l++;
	while (j < l)
	{
		a[0] = map_get(m, j, i);
		a[1] = map_get(m, j - 1, i);
		a[2] = map_get(m, j, i - 1);
		if ((!a[0] && (a[1] < 0 || a[2] < 0)) || (a[0] < 0 && (!a[1] || !a[2])))
			return (parser_error_cub());
		j++;
	}
	return (0);
}

int	parse_map_player(t_map *m, t_player *p, char c)
{
	if (c == 'O')
		p->rdeg = 0;
	else if (c == 'S')
		p->rdeg = 90;
	else if (c == 'W')
		p->rdeg = 180;
	else if (c == 'N')
		p->rdeg = 270;
	else
		return (0);
	p->pos.x = m->rows[m->l].l + 0.5;
	p->pos.y = m->l + 0.5;
	return (1);
}

int	parse_add_map_row(t_map *m, t_player *p, t_string line)
{
	size_t	i;

	i = 0;
	while (i < line.l && line.s[i] != '0' && line.s[i] != '1')
		i++;
	if (i == line.l)
		return (0);
	m->rows[m->l].l = 0;
	m->rows[m->l].a = malloc(line.l - i);
	m->rows[m->l].off = i;
	while (i < line.l)
	{
		m->rows[m->l].a[m->rows[m->l].l] = -1;
		if (line.s[i] == '1')
			m->rows[m->l].a[m->rows[m->l].l] = 1;
		else if (line.s[i] == '0' || parse_map_player(m, p, line.s[i]))
			m->rows[m->l].a[m->rows[m->l].l] = 0;
		m->rows[m->l].l++;
		i++;
	}
	if ((int)i > m->max_l)
		m->max_l = i;
	return (parse_check_map_row(m, m->l++));
}

int	parse_map(t_app *app, t_parser *p)
{
	t_string	line;
	int			r;

	line = buffer_gnl_string(&p->b);
	while (line.s && !parse_add_line(p, line))
		line = buffer_gnl_string(&p->b);
	if (line.s)
	{
		str_clear(&line);
		return (1);
	}
	app->m.max_l = 0;
	app->m.l = 0;
	app->m.rows = malloc(p->l * sizeof(t_row));
	if (!app->m.rows)
		return (parser_error_malloc());
	r = 0;
	while (!r && p->lines && p->lines->line.s)
	{
		r = parse_add_map_row(&app->m, &app->p, p->lines->line);
		parse_pop(&p->lines, 1);
	}
	while (p->lines)
		parse_pop(&p->lines, 1);
	return (r);
}
