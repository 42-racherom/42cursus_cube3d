/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/30 16:19:50 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:35:29 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>

int	parser_error_file(void)
{
	perror("Error\nParser");
	return (1);
}

int	parser_error_cub(void)
{
	write(2, "Error\nParser: invalid .cub file\n", 32);
	return (1);
}

int	parser_error_malloc(void)
{
	write(2, "Error\nParser: unable to allocate memory\n", 39);
	return (1);
}
