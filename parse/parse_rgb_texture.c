/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_rgb_texture.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/27 15:35:51 by rauer             #+#    #+#             */
/*   Updated: 2024/05/27 16:44:41 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <texture.h>

int	parse_rgb(char *str, unsigned int *rgb)
{
	unsigned int	c;

	*rgb = 0;
	c = 0;
	while (*str && c < 256)
	{
		if (*str >= '0' && *str <= '9')
		{
			c *= 10;
			c += *str - '0';
		}
		else if (*str == ',' || *str == '\n')
		{
			*rgb <<= 8;
			*rgb |= c & 0xff;
			c = 0;
		}
		else
			break ;
		str++;
	}
	if (*str || c > 255)
		return (1);
	return (0);
}

int	parse_rgb_texture(char *str, unsigned int *rgb)
{
	int	r;

	while (*str == ' ' || *str == '\t')
		str++;
	r = parse_rgb(str, rgb);
	if (r)
		return (r);
	return (0);
}
