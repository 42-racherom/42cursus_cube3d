/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/27 04:01:48 by racherom          #+#    #+#             */
/*   Updated: 2024/05/30 08:37:10 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef APP_H
# define APP_H

# include <frame.h>
# include <map.h>
# include <player.h>
# include <render.h>

typedef struct s_app
{
	int				w;
	int				h;
	t_map			m;
	t_player		p;
	t_renderer		r;
	unsigned int	f;
	int				minimap;
}					t_app;

int					app_init(t_app *app);
int					app_start(t_app *app);
int					app_hooks(t_app *app);
int					app_loop(t_app *app);
int					app_close(t_app *app);

char				*ft_ftoa(float f, unsigned int prec);

#endif
