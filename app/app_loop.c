/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_loop.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/30 07:56:43 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 14:48:41 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "app.h"
#include "keys.h"
#include <stdio.h>
#include <unistd.h>

int	handle_rotation(t_app *app)
{
	int	f;

	f = app->f & (K_ARROW_L | K_ARROW_R);
	if (f == K_ARROW_L)
		app->p.rdeg += 354;
	if (f == K_ARROW_R)
		app->p.rdeg += 6;
	if (app->p.rdeg >= 360)
		app->p.rdeg -= 360;
	return (0);
}

int	handle_movement_r(unsigned int f)
{
	if ((f & K_W) && (f & K_S))
		f &= ~(K_W | K_S);
	if ((f & K_A) && (f & K_D))
		f &= ~(K_A | K_D);
	if (!f)
		return (-1);
	if (f == (K_W | K_D))
		return (45);
	if (f == K_D)
		return (90);
	if (f == (K_D | K_S))
		return (135);
	if (f == K_S)
		return (180);
	if (f == (K_S | K_A))
		return (225);
	if (f == K_A)
		return (270);
	if (f == (K_A | K_W))
		return (315);
	return (0);
}

int	handle_movement(t_app *app)
{
	t_point_2d	p;
	int			r;

	r = handle_movement_r(app->f & (K_W | K_S | K_A | K_D));
	if (r < 0)
		return (0);
	p = app->p.pos;
	p.x += 0.1;
	if (app->f & K_CONTROL)
		p.x += 0.1;
	app->p.pos = rotate_2d(p, app->p.pos, (app->p.rdeg + r) * (M_PI / 180));
	return (0);
}

int	app_loop(t_app *app)
{
	int	r;

	r = handle_rotation(app);
	if (!r)
		r = handle_movement(app);
	if (!r)
		r = renderer_game(&app->r, &app->p, &app->m);
	if (!r && app->minimap)
		r = renderer_minimap(&app->r, &app->p, &app->m);
	if (!r)
		return (0);
	write(2, "Error\nApp unexpected return value\n", 34);
	app_close(app);
	return (1);
}
