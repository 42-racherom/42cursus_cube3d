/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/29 06:19:59 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 16:17:27 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>
#include <stdio.h>
#include <stdlib.h>

int	app_init(t_app *app)
{
	int	r;

	app->w = 800;
	app->h = 600;
	app->f = 0;
	app->minimap = 2;
	r = renderer_init(&app->r);
	if (!r)
		r = map_init(&app->m);
	if (!r)
		r = player_init(&app->p);
	return (r);
}

int	app_start(t_app *app)
{
	int	r;

	r = renderer_start(&app->r);
	if (!r && app->minimap)
		r = renderer_start_minimap(&app->r, &app->m);
	if (!r)
		r = app_hooks(app);
	if (!r)
		mlx_loop_hook(app->r.mlx, app_loop, app);
	return (mlx_loop(app->r.mlx));
}

int	app_close(t_app *app)
{
	renderer_clear(&app->r);
	map_clear(&app->m);
	player_clear(&app->p);
	exit(0);
}
