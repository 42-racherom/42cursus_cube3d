/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_hooks.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/30 07:49:33 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 13:32:19 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "app.h"
#include "keys.h"
#include <map.h>
#include <mlx.h>
#include <stdio.h>
#include <stdlib.h>

int	translate_key(int keycode)
{
	if (keycode == KEY_CONTROL)
		return (K_CONTROL);
	if (keycode == KEY_W)
		return (K_W);
	if (keycode == KEY_A)
		return (K_A);
	if (keycode == KEY_S)
		return (K_S);
	if (keycode == KEY_D)
		return (K_D);
	if (keycode == KEY_ARROW_L)
		return (K_ARROW_L);
	if (keycode == KEY_ARROW_R)
		return (K_ARROW_R);
	return (-1);
}

int	key_down_hook(int keycode, t_app *app)
{
	int	k;

	k = translate_key(keycode);
	if (k > 0)
		app->f |= k;
	return (0);
}

int	key_up_hook(int keycode, t_app *app)
{
	int	k;

	k = translate_key(keycode);
	if (k > 0)
		app->f &= ~k;
	else if (keycode == KEY_ESC)
		return (app_close(app));
	else if (keycode == KEY_M)
		app->f |= KEYFLAG_MAP;
	return (0);
}

int	app_hooks(t_app *app)
{
	mlx_hook(app->r.w_game, EVENT_KeyPress, 1, key_down_hook, app);
	mlx_hook(app->r.w_game, EVENT_KeyRelease, 2, key_up_hook, app);
	mlx_hook(app->r.w_game, EVENT_DestroyNotify, 0, app_close, app);
	if (!app->minimap)
		return (0);
	mlx_hook(app->r.w_minimap, EVENT_KeyPress, 1, key_down_hook, app);
	mlx_hook(app->r.w_minimap, EVENT_KeyRelease, 2, key_up_hook, app);
	mlx_hook(app->r.w_minimap, EVENT_DestroyNotify, 0, app_close, app);
	return (0);
}
