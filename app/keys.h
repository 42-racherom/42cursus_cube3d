/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbaldoni <dbaldoni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/09 22:13:12 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 15:26:25 by dbaldoni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYS_H
# define KEYS_H
# ifdef __linux__

typedef enum e_key
{
	KEY_CONTROL = 65507,
	KEY_ESC = 65307,
	KEY_W = 0x0077,
	KEY_A = 0x0061,
	KEY_S = 0x0073,
	KEY_D = 0x0064,
	KEY_ARROW_L = 65361,
	KEY_ARROW_R = 65363,
	KEY_M = 46,
	KEY_C = 0x0063
}	t_key;
# else

typedef enum e_key
{
	KEY_CONTROL = 256,
	KEY_ESC = 53,
	KEY_W = 0x000d,
	KEY_A = 0x0000,
	KEY_S = 0x0001,
	KEY_D = 0x0002,
	KEY_ARROW_L = 123,
	KEY_ARROW_R = 124,
	KEY_M = -1,
	KEY_C = 0x0063
}	t_key;
# endif

typedef enum e_keyflag
{
	K_W = 0b000000001,
	K_A = 0b000000010,
	K_S = 0b000000100,
	K_D = 0b000001000,
	K_CONTROL = 0b000010000,
	K_ARROW_L = 0b000100000,
	K_ARROW_R = 0b001000000,
	KEYFLAG_MAP = 0b010000000,
	KEYFLAG_MOUSE = 0b100000000
}	t_keyflag;

typedef enum e_event
{
	EVENT_KeyPress = 2,
	EVENT_KeyRelease = 3,
	EVENT_MouseDown = 4,
	EVENT_MouseUp = 5,
	EVENT_MouseMove = 6,
	EVENT_DestroyNotify = 17,
	EVENT_ResizeRequest = 25
}	t_event;

#endif
