/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/27 05:05:38 by racherom          #+#    #+#             */
/*   Updated: 2024/05/30 16:01:54 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <app.h>
#include <parser.h>
#include <stdio.h>
#include <unistd.h>

int	main_error_arguments(void)
{
	write(2, "Error\nInvalid number of arguments\n", 33);
	return (1);
}

int	main(int argc, char **argv)
{
	t_app	app;
	int		r;

	if (argc != 2)
		return (main_error_arguments());
	r = app_init(&app);
	if (!r)
		r = parse_cub(&app, argv[1]);
	if (!r)
		r = app_start(&app);
	app_close(&app);
	return (1);
}
