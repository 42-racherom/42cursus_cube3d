# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dbaldoni <dbaldoni@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/03/22 20:19:47 by rauer             #+#    #+#              #
#    Updated: 2024/05/30 15:18:54 by dbaldoni         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = cub3D
SRC = main.c
OBJ = $(SRC:.c=.o)
LIB = parse buffer str app render player map frame texture point mlx
ifeq ($(shell uname), Linux)
	MLX = https://cdn.intra.42.fr/document/document/23215/minilibx-linux.tgz
else
	MLX = https://cdn.intra.42.fr/document/document/23216/minilibx_opengl.tgz
endif
ifeq ($(shell uname), Linux)
OS_FLAGS = -L/usr/lib -lXext -lX11 -lz
OS = linux
else
OS_FLAGS = -framework OpenGL -framework AppKit
OS = macos
endif
INC = -I . $(addprefix -I , $(LIB))
LIBS = $(addprefix libs/lib, $(addsuffix .a, $(LIB)))
LIB_FLAGS =  -L libs $(addprefix -l, $(LIB)) -lm
GCC = gcc -Wall -Wextra -Werror

all: mlx $(LIBS) $(NAME)

$(NAME): mlx $(OBJ) $(LIBS)
	$(GCC) $(OBJ) $(DEBUG) $(INC) $(LIB_FLAGS) $(OS_FLAGS) -o $@

.PHONY: all clean fclean mlx_macos

FORCE: ;

%.o: %.c
	$(GCC) $(DEBUG) $(INC) -c $< -o $@

mlx:
	wget $(MLX)
	tar -xzf $(notdir $(MLX))
	rm $(notdir $(MLX))
	mkdir mlx
	mv minilibx*/* mlx

libs/lib%.a::% libs FORCE
	@$(MAKE) -C $* DEBUG="$(DEBUG)"
	cp -p $*/*.a libs

libs:
	mkdir libs

clean_%:
	-$(MAKE) -iC $* clean

clean: $(addprefix clean_, $(LIB))
	rm -rf $(OBJ)


fclean_%:
	-$(MAKE) -ikC $* fclean
	rm -rf mlx

fclean: clean $(addprefix fclean_, $(LIB))
	rm -rf $(NAME)
	rm -rf libs

test:
	echo $(LIBS)

re: fclean all
