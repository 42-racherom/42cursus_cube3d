/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/28 22:37:20 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 09:47:10 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ray.h"
#include <map.h>
#include <point.h>

t_ray	cast_ray(t_map *map, t_ray ray, t_point_2d off)
{
	int	i;
	int	r;

	r = map_get(map, ray.b.max.x, ray.b.max.y);
	i = 0;
	while (!r && i++ < 100)
	{
		ray.b.max = offset_point_2d(ray.b.max, off);
		r = map_get(map, ray.b.max.x, ray.b.max.y);
	}
	if (r > 0)
		ray.d = distance_2d(ray.b.min, ray.b.max);
	return (ray);
}

t_ray	horizontal_ray(t_map *map, t_point_2d p, float deg, float tan_deg)
{
	t_ray		ray;
	t_point_2d	off;

	tan_deg = -1 / tan_deg;
	off.y = 1;
	ray.o = S;
	ray.b.min = p;
	ray.d = 1000000;
	if (deg > 180)
	{
		ray.b.max.y = floorf(p.y) - 0.00001;
		off.y = -1;
		ray.o = N;
	}
	else if (deg > 0 && deg < 180)
		ray.b.max.y = floorf(p.y) + 1;
	else
		return (ray);
	ray.b.max.x = (p.y - ray.b.max.y) * tan_deg + p.x;
	off.x = -off.y * tan_deg;
	return (cast_ray(map, ray, off));
}

t_ray	vertical_ray(t_map *map, t_point_2d p, float deg, float tan_deg)
{
	t_ray		ray;
	t_point_2d	off;

	tan_deg = -tan_deg;
	off.x = 1;
	ray.o = E;
	ray.b.min = p;
	ray.d = 1000000;
	if (deg > 90 && deg < 270)
	{
		ray.b.max.x = floorf(p.x) - 0.00001;
		off.x = -1;
		ray.o = W;
	}
	else if (deg < 90 || deg > 270)
		ray.b.max.x = floorf(p.x) + 1;
	else
		return (ray);
	ray.b.max.y = (p.x - ray.b.max.x) * tan_deg + p.y;
	off.y = -off.x * tan_deg;
	return (cast_ray(map, ray, off));
}

t_ray	cast_rays(t_map *map, t_point_2d p, float deg)
{
	t_ray	ray_h;
	t_ray	ray_v;
	float	tan_deg;

	tan_deg = tanf(deg * M_PI / 180.0);
	ray_h = horizontal_ray(map, p, deg, tan_deg);
	ray_v = vertical_ray(map, p, deg, tan_deg);
	ray_v.b.min.c = 0x99FF00;
	ray_h.b.min.c = 0x4a7a01;
	if (ray_h.d < 0)
		return (ray_v);
	if (ray_v.d < 0 || ray_h.d < ray_v.d)
		return (ray_h);
	return (ray_v);
}
