/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/29 02:39:59 by rauer             #+#    #+#             */
/*   Updated: 2024/05/29 02:42:08 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RAY_H
# define RAY_H

# include <map.h>
# include <point.h>

typedef struct s_ray
{
	t_bound_2d	b;
	float		d;
	int			shadow;
	int			o;
}				t_ray;

t_ray			cast_rays(t_map *map, t_point_2d p, float deg);

#endif
