/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/13 18:06:44 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 08:32:37 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_H
# define MAP_H

typedef enum e_orientation
{
	N = 0,
	E = 1,
	S = 2,
	W = 3
}			t_orientation;

typedef struct s_row
{
	char	*a;
	int		off;
	int		l;
}			t_row;

typedef struct s_map
{
	t_row	*rows;
	int		l;
	int		max_l;
}			t_map;

int			map_init(t_map *m);
int			map_get(t_map *map, int x, int y);
void		map_clear(t_map *map);

#endif
