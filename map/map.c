/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/10 16:31:45 by rauer             #+#    #+#             */
/*   Updated: 2024/05/30 14:59:08 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <map.h>
#include <stdio.h>
#include <stdlib.h>

int	map_init(t_map *m)
{
	m->l = 0;
	m->rows = 0;
	m->max_l = 0;
	return (0);
}

int	map_get(t_map *m, int x, int y)
{
	if (y < 0 || y >= m->l)
		return (-1);
	x -= m->rows[y].off;
	if (x < 0 || x >= m->rows[y].l)
		return (-1);
	return (m->rows[y].a[x]);
}

void	map_clear(t_map *m)
{
	int	i;

	i = 0;
	while (i < m->l)
		free(m->rows[i++].a);
	free(m->rows);
}
