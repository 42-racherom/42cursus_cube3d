/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_int.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/19 21:32:15 by rauer             #+#    #+#             */
/*   Updated: 2024/05/29 03:58:31 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "str.h"
#include <stdio.h>

int	str_int_len(int i)
{
	int	j;

	j = 1;
	if (i < 0)
		j++;
	while (i > 9 || i < -9)
	{
		i /= 10;
		j++;
	}
	return (j);
}

t_string	str_itoa(int i)
{
	t_string	str;
	int			l;

	l = str_int_len(i);
	str = str_new(l);
	if (!str.s)
		return (str);
	if (i < 0)
	{
		str.s[0] = '-';
		str.s[--l] = '0' - i % 10;
		i = -(i / 10);
	}
	while (i > 9)
	{
		str.s[--l] = '0' + (i % 10);
		i /= 10;
	}
	str.s[--l] = '0' + (i % 10);
	str.l = str.c;
	str.s[str.l] = 0;
	return (str);
}

int	str_atoi(t_string str)
{
	(void)str;
	return (0);
}
