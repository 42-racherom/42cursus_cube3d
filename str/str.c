/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/27 02:36:14 by racherom          #+#    #+#             */
/*   Updated: 2024/05/29 03:58:31 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "str.h"

int	str_check(t_string str)
{
	if (str.l > str.c || (str.c > 0 && str.s == 0))
		return (0);
	return (1);
}

int	str_copy_to(t_string *str1, t_string str2, int from, int to)
{
	size_t	i;

	i = str2.l;
	if (to > from)
		i = to;
	i -= from;
	str1->l = 0;
	if (str1->c < i)
	{
		free(str1->s);
		*str1 = str_new(i);
	}
	if (!str1->s)
		return (1);
	while (str1->l < i)
		str1->s[str1->l++] = str2.s[from++];
	str1->s[str1->l] = 0;
	return (0);
}

t_string	str_copy(t_string str1, int from, int to)
{
	t_string	str2;

	str2 = str_make(0);
	str_copy_to(&str2, str1, from, to);
	return (str2);
}

int	str_move(t_string *str1, t_string *str2)
{
	str_clear(str1);
	*str1 = *str2;
	*str2 = str_make(0);
	return (0);
}

int	str_join(t_string *str1, t_string str2, char c)
{
	size_t	i;

	if (!str_check(*str1) || !str_check(str2))
		return (1);
	if (str1->c < str1->l + str2.l + 1 && str_extend(str1, str2.l + 1))
		return (1);
	if (c)
		str1->s[str1->l++] = c;
	i = 0;
	while (i < str2.l)
		str1->s[str1->l++] = str2.s[i++];
	str1->s[str1->l] = 0;
	return (0);
}
