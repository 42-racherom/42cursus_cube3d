/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/27 02:36:14 by racherom          #+#    #+#             */
/*   Updated: 2024/05/29 03:58:31 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "str.h"

int	str_cmp(t_string str1, t_string str2)
{
	size_t	i;

	if (str1.s == str2.s)
		return (0);
	if (str1.l != str2.l)
		return (1);
	i = 0;
	while (i < str1.l && str1.s[i] == str2.s[i])
		i++;
	return ((unsigned char)str1.s[i] - (unsigned char)str2.s[i]);
}

t_string	str_break(t_string *str1, size_t i)
{
	t_string	str2;

	str2 = str_copy(*str1, i, 0);
	if (str2.s)
	{
		str1->s[i] = 0;
		if (i > str1->l)
			str1->l = i;
	}
	return (str2);
}

int	str_replace(t_string *str1, t_string str2, size_t i, size_t l)
{
	size_t		j;
	t_string	tmp;

	tmp = str_new(str1->l + str2.l - l);
	if (!tmp.s)
		return (1);
	j = 0;
	while (j < i)
		tmp.s[tmp.l++] = str1->s[j++];
	j = tmp.c;
	while (str1->l >= i && j >= i)
		tmp.s[j--] = str1->s[str1->l--];
	j = 0;
	while (j < str2.l)
		tmp.s[i++] = str2.s[j++];
	tmp.l = tmp.c;
	tmp.s[tmp.l] = 0;
	free(str1->s);
	*str1 = tmp;
	return (0);
}

int	ft_is_whitespace(char c)
{
	return (c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f'
		|| c == '\r');
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	int		j;
	char	*str;

	if (!s1 || !s2)
		return (0);
	str = malloc(ft_strlen(s1) + ft_strlen(s2) + 1);
	if (!str)
		return (NULL);
	i = 0;
	j = 0;
	while (s1[i])
	{
		str[i] = s1[i];
		i++;
	}
	while (s2[j])
	{
		str[i] = s2[j];
		i++;
		j++;
	}
	str[i] = '\0';
	return (str);
}
