/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/27 02:36:08 by racherom          #+#    #+#             */
/*   Updated: 2024/05/30 10:33:41 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STR_H
# define STR_H
# include <stddef.h>
# include <stdlib.h>

typedef struct s_string
{
	char		*s;
	size_t		l;
	size_t		c;
}				t_string;

typedef struct s_var
{
	int			i;
	int			word;
	int			len;
}				t_var;

typedef struct s_str_find
{
	size_t		i;
	size_t		j;
	t_string	s;
}				t_str_find;

t_string		str_itoa(int i);
t_string		str_new(int c);
t_string		str_make(char *s);
t_string		str_make_dup(char *s);
void			str_clear(t_string *s);
int				str_move(t_string *str1, t_string *str2);
t_string		str_break(t_string *str1, size_t i);
int				str_replace(t_string *str1, t_string str2, size_t i, size_t l);
t_string		str_copy(t_string str1, int from, int to);
int				str_copy_to(t_string *str1, t_string str2, int from, int to);
int				str_extend(t_string *str, size_t i);
int				str_join(t_string *str1, t_string str2, char c);
int				str_putchar(t_string *str, char c);
char			**ft_split(char const *s, char c);
char			*ft_substr(char const *s, unsigned int start, size_t len);
int				str_cmp(t_string str1, t_string str2);
int				ft_strcmp(const char *s1, const char *s2);
size_t			ft_strlen(const char *s);
char			*ft_strjoin(char const *s1, char const *s2);
char			*ft_strdup(const char *s1);
int				ft_is_whitespace(char c);

#endif
