/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_new.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/19 21:29:59 by rauer             #+#    #+#             */
/*   Updated: 2024/05/29 03:58:31 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "str.h"
#include <stdio.h>

void	str_clear(t_string *s)
{
	if (s->s)
		free(s->s);
	s->s = 0;
	s->l = 0;
	s->c = 0;
}

t_string	str_new(int c)
{
	t_string	str;

	str.l = 0;
	str.c = c;
	str.s = 0;
	if (c > 0)
		str.s = malloc(c + 1);
	if (str.s)
		*(str.s) = 0;
	else
		str.c = 0;
	return (str);
}

t_string	str_make(char *s)
{
	t_string	str;

	str.s = s;
	str.l = 0;
	while (str.s && str.s[str.l])
		str.l++;
	str.c = str.l;
	return (str);
}

t_string	str_make_dup(char *s)
{
	t_string	str;
	size_t		i;

	str.l = 0;
	str.c = 0;
	i = 0;
	while (s[i])
		i++;
	str.s = malloc(i + 1);
	if (!str.s)
		return (str);
	str.c = i;
	i = 0;
	while (s[i] && i < str.c)
		str.s[str.l++] = s[i++];
	str.s[str.l] = 0;
	return (str);
}

int	str_extend(t_string *str, size_t i)
{
	char	*tmp;
	size_t	j;

	tmp = malloc(str->c + i + 1);
	if (!tmp)
		return (1);
	j = 0;
	while (j < str->l)
	{
		tmp[j] = str->s[j];
		j++;
	}
	tmp[j] = 0;
	free(str->s);
	str->s = tmp;
	str->c += i;
	return (0);
}
